# Recipistr

*Recipes over nostr*

- Find recipes by ingredients
- Tip the author of your favourite recipes
- Create recipes and, if people like them, receive tips to your LN Address


## Getting started

Clone the project
```
git clone https://codeberg.org/recipistr/recipistr.git
cd recipistr
```

Optionally edit `Rocket.toml` to configure.

Run it
```
cargo run
```

Recipistr is running at http://127.0.0.1:8081

For more information, read [The Guide](GUIDE.md)