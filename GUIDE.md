# Guide

## Configure Recipistr

Open `Rocket.toml` to change
* the relay used
* your nostr private key


## First run

By default, Recipistr creates a new random nostr account each time it starts.

If you already have a nostr private key and wish to use it, see above how to add it in `Rocket.toml`.


## Add LN address to your account

If you wish to be able to receive LN tips by people browsing your recipes, make sure to add a LN Address to your nostr profile:

- Go to your profile page
- Add or change your LN Address


## Browse recipes

To browse recipes, open the main page and
- Click on one ingredient on the left, to see recipes using that ingredient
- Click on more ingredients to add or remove


#### How to tip recipe author

If you are on the page of a recipe you like, and you want to tip the recipe's author:

- Click on the pukbey of the author, to open the author page 
- If the author has a LN Address configured, you will see a QR code
- Scan the QR code with a LN wallet to send the tip


## Create recipe

- Click the 'New recipe' link on top
- Fill out the form
- Click submit


## Anti-spam measures

To avoid spam, Recipistr uses Proof-of-Work:

1. When creating the recipe
2. When user views existing recipes

Both have the default of 10 bits of PoW and can be configured in `Rocket.toml`
