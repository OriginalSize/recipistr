use crate::model::forms::*;
use crate::model::nostr::*;
use crate::Config;
use anyhow::{anyhow, Result};
use nostr::types::metadata::Metadata;
use nostr_sdk::prelude::rand::thread_rng;
use nostr_sdk::prelude::*;
use rocket::log;
use std::collections::HashMap;
use std::time::Duration;
use tokio::time::sleep;

const KIND_RECIPE: u64 = 8000;

pub(crate) async fn create_client(config: &Config) -> Result<Client> {
    let secret_key = match &config.secret_key {
        None => SecretKey::new(&mut thread_rng()),
        Some(secret_key_str) => SecretKey::from_bech32(secret_key_str)?,
    };

    let my_keys = Keys::new(secret_key);
    log::info_!(
        "Recipistr starting with nostr pubkey {}",
        my_keys.public_key().to_bech32()?
    );

    let client = Client::new(&my_keys);
    for relay in &config.relays {
        client.add_relay(relay, None).await?;
    }
    client.connect().await;
    Ok(client)
}

pub(crate) async fn is_at_least_one_relay_working(client: &Client) -> bool {
    match tokio::time::timeout(
        Duration::from_secs(2),
        client.get_events_of(
            vec![Filter::new().kind(Kind::Custom(KIND_RECIPE)).limit(1)],
            Some(Duration::from_secs(1)),
        ),
    )
    .await
    {
        Ok(_) => true,
        Err(_) => {
            println!("did not receive value within 2 s");
            false
        }
    }
}

fn create_recipe_event(
    config: &Config,
    keys: Keys,
    title: String,
    notes: String,
    ingredients: Vec<FormIngredient>,
    steps: Vec<FormStep>,
    duration_mins: u32,
) -> Result<Event> {
    // Convert from FormIngredient to Ingredient
    let recipe_ingredients: Vec<Ingredient> = ingredients
        .iter()
        .map(|fi| Ingredient {
            amount: fi.ingredient_amount,
            unit: fi.ingredient_unit.to_string(),
            kind: fi.ingredient_kind.to_string(),
        })
        .collect();
    let recipe_steps: Vec<String> = steps.iter().map(|s| s.step.to_string()).collect();

    let recipe = Recipe {
        title,
        notes,
        ingredients: recipe_ingredients,
        steps: recipe_steps,
        duration_mins,
    };
    let json = serde_json::to_string(&recipe)?;

    EventBuilder::new(Kind::Custom(KIND_RECIPE), json, &[])
        .to_pow_event(&keys, config.pow_new_recipes)
        .map_err(|e| anyhow!(e))
}

pub(crate) async fn create_recipe_and_post_event<'v>(
    config: &Config,
    client: &Client,
    title: &'v str,
    notes: &'v str,
    ingredients: Vec<FormIngredient<'v>>,
    steps: Vec<FormStep<'v>>,
    duration_mins: u32,
) -> Result<()> {
    let event = create_recipe_event(
        config,
        client.keys(),
        title.to_string(),
        notes.to_string(),
        ingredients,
        steps,
        duration_mins,
    )?;
    client.send_event(event).await?;
    sleep(Duration::from_millis(500)).await; // TODO Investigate why it needs 1s to show recipe after creating
    Ok(())
}

/// When the author_pk is specified method returns all recipes of that author, otherwise returns all recipes of all authors
pub(crate) async fn get_all_recipes(
    client: &Client,
    config: &Config,
    author_pk: Option<XOnlyPublicKey>,
) -> Result<Vec<(String, Recipe)>> {
    let prefixes = get_prefixes_for_difficulty(config.pow_reading_recipes);
    let mut filter = Filter::new().kind(Kind::Custom(KIND_RECIPE)).ids(prefixes);
    // If the method is called with an author pk, we add the pk to the subscription filter
    if let Some(auth_pk) = author_pk {
        filter = filter.author(auth_pk.to_string());
    }
    let mut result: Vec<(String, Recipe)> = vec![];

    let res_events = client
        .get_events_of(vec![filter], Some(Duration::from_secs(2)))
        .await?;
    let res_events_no_duplicates = remove_duplicate_events(res_events);
    for event in res_events_no_duplicates {
        if event.kind.as_u64() == KIND_RECIPE {
            let id = event.id.to_bech32()?;
            let r = serde_json::from_str::<Recipe>(&event.content);
            match r {
                Ok(r) => {
                    result.push((id, r));
                }
                Err(e) => {
                    println!("Failed to convert floats to integers due to {e}");
                }
            }
        }
    }

    Ok(result)
}

fn remove_duplicate_events(res_events: Vec<Event>) -> Vec<Event> {
    let mut hmap = HashMap::new();
    for event in res_events {
        hmap.insert(event.id, event.clone());
    }
    hmap.values().cloned().collect()
}

pub(crate) async fn get_recipe(
    client: &Client,
    id: EventId,
) -> Result<Option<(XOnlyPublicKey, Recipe)>> {
    let filter = Filter::new().kind(Kind::Custom(KIND_RECIPE)).id(id);
    let mut result: Option<(XOnlyPublicKey, Recipe)> = None;

    let result_events = client
        .get_events_of(vec![filter], Some(Duration::from_secs(3)))
        .await?;
    for event in result_events {
        if event.kind.as_u64() == KIND_RECIPE {
            let r = serde_json::from_str::<Recipe>(&event.content);
            match r {
                Ok(r) => result = Some((event.pubkey, r)),
                Err(e) => {
                    println!("Failed to decode Json: {e}");
                }
            }
        }
    }
    Ok(result)
}

/// Errors:
/// - when it can't connect to relay
/// Option(None) when:
/// - profile has no LUD16
/// - when LUD16 is empty/has only white spaces
/// - when content of LUD16 has LN address format
pub(crate) async fn get_ln_address(
    client: &Client,
    author_pk: XOnlyPublicKey,
) -> Result<Option<String>> {
    match get_metadata(client, author_pk).await? {
        None => Ok(None),
        Some(metadata) => Ok(metadata.lud16),
    }
}

async fn get_metadata(client: &Client, pk: XOnlyPublicKey) -> Result<Option<Metadata>> {
    let filter = Filter::new().kind(Kind::Metadata).author(pk.to_string());

    let res_event = client
        .get_events_of(vec![filter], Some(Duration::from_secs(3)))
        .await?;
    if let Some(event) = res_event.into_iter().next() {
        let metadata = Metadata::from_json(event.content);
        return match metadata {
            Ok(m) => Ok(Some(m)),
            Err(e) => {
                println!("Failed to obtain metadata due to {e}");
                Err(anyhow!(e.to_string()))
            }
        };
    }
    Ok(None) // user has no metadata
}

pub(crate) async fn update_ln_address(client: &Client, new_ln_address: String) -> Result<()> {
    let modified_metadata = match get_metadata(client, client.keys().public_key()).await? {
        None => {
            // create new metadata with LN in it
            Metadata::new().lud16(new_ln_address)
        }
        Some(current_metadata) => {
            // update existing metadata with new LN address
            current_metadata.lud16(new_ln_address)
        }
    };

    client.set_metadata(modified_metadata).await?;
    Ok(())
}
