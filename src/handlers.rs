use core::cmp::max;
use nostr_sdk::key::XOnlyPublicKey;
use nostr_sdk::prelude::{FromBech32, ToBech32};
use nostr_sdk::{Client, EventId};
use rocket::form::{Context, Contextual, Form};
use rocket::http::Status;
use rocket::{log, Request, State};
use std::collections::HashSet;

use crate::model::forms::{NewRecipeSubmit, UpdateLnAddressSubmit};
use crate::nostr::is_at_least_one_relay_working;
use crate::Config;
use rocket_dyn_templates::{context, Template};

/// Called when new ingredients or new steps are added on the New Recipe page
#[post(
    "/new_recipe_update?<number_of_ingredients>&<number_of_steps>",
    data = "<form>"
)]
pub(crate) async fn update_currently_created_recipe<'r>(
    form: Form<Contextual<'r, NewRecipeSubmit<'r>>>,
    number_of_ingredients: Option<u8>,
    number_of_steps: Option<u8>,
    client: &State<Client>,
) -> Template {
    if !is_at_least_one_relay_working(client).await {
        return get_error_template();
    }

    Template::render(
        "new_recipe",
        context! {
            title: "New recipe page",
            number_of_ingredients: max(1, number_of_ingredients.unwrap_or(1)),
            number_of_steps: max(1, number_of_steps.unwrap_or(1)),
            form_ctx: &form.context
        },
    )
}

#[post("/?<number_of_ingredients>&<number_of_steps>", data = "<form>")]
pub(crate) async fn submit_new_recipe<'r>(
    form: Form<Contextual<'r, NewRecipeSubmit<'r>>>,
    number_of_ingredients: Option<u8>,
    number_of_steps: Option<u8>,
    client: &State<Client>,
    config: &State<Config>,
) -> (Status, Template) {
    if !is_at_least_one_relay_working(client).await {
        return (Status::Ok, get_error_template());
    }

    let template = match form.value {
        Some(ref submission) => {
            crate::nostr::create_recipe_and_post_event(
                config,
                client,
                submission.r#title,
                submission.notes,
                submission.r#form_ingredients.clone(),
                submission.form_steps.clone(),
                submission.duration_mins,
            )
            .await
            .expect("Failed to insert recipe"); // TODO Handle error, avoid expect()

            Template::render("success", &form.context)
        }
        // In case of form validation error re-render the form page with errors in the context
        None => Template::render(
            "new_recipe",
            context! {
                title: "New recipe page",
                number_of_ingredients: max(1, number_of_ingredients.unwrap_or(1)),
                number_of_steps: max(1, number_of_steps.unwrap_or(1)),
                form_ctx: &form.context,
            },
        ),
    };
    (form.context.status(), template)
}

#[get("/?<s>")]
pub async fn index(
    s: Option<Vec<String>>,
    client: &State<Client>,
    config: &State<Config>,
) -> Template {
    if !is_at_least_one_relay_working(client).await {
        return get_error_template();
    }

    let tuples = crate::nostr::get_all_recipes(client, config, None)
        .await
        .expect("Failed to get recipes");
    let ingredient_kinds = tuples
        .iter()
        .map(|tuple| &tuple.1)
        .flat_map(|recipe| {
            recipe
                .ingredients
                .iter()
                .map(|ing| ing.kind.clone())
                .collect::<Vec<String>>()
        })
        .collect::<HashSet<String>>();

    let mut sorted_ingredient_kinds: Vec<String> = ingredient_kinds.into_iter().collect();
    sorted_ingredient_kinds.sort();

    let processed_tuples = match &s {
        None => {
            // return full list of tuples
            tuples
        }
        Some(ingredients_searched_by_user) => {
            // return the list of recipes containing all of ingredients_searched_by_user
            tuples
                .iter()
                .filter(|tuple| {
                    // Check if all user selected ingredients are present in this recipe's ingredients
                    ingredients_searched_by_user
                        .iter()
                        .all(|user_selected_ingredient| {
                            tuple
                                .1
                                .ingredients
                                .iter()
                                .any(|ingredient| &ingredient.kind == user_selected_ingredient)
                        })
                })
                .cloned()
                .collect()
        }
    };

    Template::render(
        "index",
        context! {
            title: "Homepage",
            tuples: processed_tuples,
            ingredient_kinds: sorted_ingredient_kinds,
            searched_ingredients: s
        },
    )
}

#[get("/new_recipe")]
pub async fn new_recipe(client: &State<Client>) -> Template {
    if !is_at_least_one_relay_working(client).await {
        return get_error_template();
    }

    Template::render(
        "new_recipe",
        context! {
            title: "New recipe page",
            number_of_ingredients: 1,
            number_of_steps: 1,
            form_ctx: &Context::default()
        },
    )
}

#[get("/about")]
pub fn about() -> Template {
    Template::render(
        "about",
        context! {
            title: "About",
        },
    )
}

#[catch(404)]
pub fn not_found(req: &Request<'_>) -> Template {
    Template::render(
        "error/404",
        context! {
            uri: req.uri()
        },
    )
}

#[get("/recipe/<id>")]
pub async fn recipe(id: String, client: &State<Client>) -> Template {
    if !is_at_least_one_relay_working(client).await {
        return get_error_template();
    }

    let pk_and_recipe = match EventId::from_bech32(id.clone()) {
        Ok(recipe_id) => crate::nostr::get_recipe(client, recipe_id)
            .await
            .expect("Failed to get recipe")
            .map(|(pk, r)| (pk.to_bech32().expect("Failed to decode pk"), r)),
        Err(_) => None,
    };

    Template::render(
        "recipe",
        context! {
            title: " recipe page",
            pk_and_recipe,
            recipe_id: id,
        },
    )
}

#[get("/author_profile/<author_npub>")]
pub async fn author_profile(
    author_npub: String,
    client: &State<Client>,
    config: &State<Config>,
) -> Template {
    if !is_at_least_one_relay_working(client).await {
        return get_error_template();
    }

    // If the id is not valid public key I want to show an error
    let ids_and_recipes_of_author = match XOnlyPublicKey::from_bech32(&*author_npub) {
        Ok(author_pk) => crate::nostr::get_all_recipes(client, config, Some(author_pk))
            .await
            .ok(),
        Err(_) => None,
    };

    let lud16 = match XOnlyPublicKey::from_bech32(author_npub.clone()) {
        Ok(author_pk) => crate::nostr::get_ln_address(client, author_pk)
            .await
            .ok()
            .flatten(),
        Err(_) => None,
    };

    let user_npub = client
        .keys()
        .public_key()
        .to_bech32()
        .unwrap_or("".to_string());
    log::info_!("Lud16 is {lud16:?}");
    Template::render(
        "author_profile",
        context! {
            title: " recipe page",
            author_npub,
            user_npub,
            ids_and_recipes_of_author,
            lud16
        },
    )
}

#[post("/user_profile_updated_LN", data = "<form>")]
pub(crate) async fn update_user_ln_address<'r>(
    form: Form<Contextual<'r, UpdateLnAddressSubmit<'r>>>,
    client: &State<Client>,
) -> (Status, Template) {
    if !is_at_least_one_relay_working(client).await {
        return (Status::Ok, get_error_template());
    }

    let template = match form.value {
        Some(ref submission) => {
            match crate::nostr::update_ln_address(client, submission.new_ln_address.into()).await {
                Ok(_) => {
                    // renders success page so no need to add anything
                    log::warn_!("Successfuly updated ln address");
                }
                Err(_e) => {
                    // re-renders page with errors in the context - no need to add anything
                    log::warn_!("Did NOT update ln address");
                }
            }

            Template::render("success", &form.context)
        }
        // In case of form validation error re-render the form page with errors in the context
        None => Template::render(
            "user_profile",
            context! {
                    title: "user_profile",
                    lud16: "",
                    form_ctx: &form.context
            },
        ),
    };

    (form.context.status(), template)
}

#[get("/user_profile")]
pub async fn user_profile(client: &State<Client>) -> Template {
    if !is_at_least_one_relay_working(client).await {
        return get_error_template();
    }

    let user_pk = client.keys().public_key();
    let lud16 = crate::nostr::get_ln_address(client, user_pk)
        .await
        .ok()
        .flatten();

    Template::render(
        "user_profile",
        context! {
            title: "user_profile",
            lud16,
            form_ctx: &Context::default()
        },
    )
}

fn get_error_template() -> Template {
    Template::render(
        "error",
        context! {
            title: "Error"
        },
    )
}
