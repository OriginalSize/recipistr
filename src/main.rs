#[macro_use]
extern crate rocket;

mod handlers;
mod model;
mod nostr;

use crate::nostr::create_client;
use rocket::error::ErrorKind;
use rocket::fairing::AdHoc;
use rocket::fs::{relative, FileServer};
use rocket_dyn_templates::Template;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct Config {
    pow_new_recipes: u8,
    pow_reading_recipes: u8,
    relays: Vec<String>,
    secret_key: Option<String>,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            pow_new_recipes: 15,
            pow_reading_recipes: 10,
            relays: vec!["wss://relay.damus.io".to_string()],
            secret_key: None,
        }
    }
}

#[rocket::main]
async fn main() -> Result<(), rocket::Error> {
    let rocket_not_launched = rocket::build()
        .mount(
            "/",
            routes![
                handlers::index,
                handlers::about,
                handlers::new_recipe,
                handlers::submit_new_recipe,
                handlers::update_currently_created_recipe,
                handlers::recipe,
                handlers::author_profile,
                handlers::user_profile,
                handlers::update_user_ln_address
            ],
        )
        .register("/", catchers![handlers::not_found])
        .attach(Template::fairing())
        .attach(AdHoc::config::<Config>())
        .mount("/", FileServer::from(relative!("/static")));

    let figment = rocket_not_launched.figment();
    let config: Config = figment.extract().expect("Failed to read config");

    let client = create_client(&config).await.map_err(|e| {
        rocket::Error::from(ErrorKind::Bind(std::io::Error::new(
            std::io::ErrorKind::Other,
            e.to_string(),
        )))
    })?;

    let _rocket_launched = rocket_not_launched.manage(client).launch().await?;

    Ok(())
}
